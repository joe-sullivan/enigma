#!/usr/bin/env python3

import string # used to get alphabet
import sys

class Machine:
	VERBOSE = True

	def __init__(self):
		self.__rotors = []
		self.__reflector = None
		self.__plugboard = Plugboard()

	@staticmethod
	def alphabet():
		return string.ascii_uppercase

	@staticmethod
	def alphabet_map(mapping, alphabet=None):
		if not alphabet: # use machine alphabet
			alphabet = Machine.alphabet()
		return [ alphabet.index(c) for c in mapping ]
		# return [ (ord(c)%32) -1 for c in mapping ]

	@staticmethod
	def map(x, alphabet=None):
		# convert between symbol and index
		if not alphabet: # used machine alphabet
			alphabet = Machine.alphabet()
		if type(x) is int: # return character
			return alphabet[x]
		try: # return index
			return alphabet.index(x)
		except:
			return None

	def offset(self, *args):
		if len(args) < len(self.__rotors):
			print('WARN: may be missing rotor position')
		positions = [p.upper() for p in args] # ensure positions are uppercase
		for p,r in zip(positions, self.__rotors):
			if p not in Machine.alphabet():
				continue
			while Machine.map(r.offset) != p:
				r.rotate()

	def ringsettings(self, *args):
		print('TODO: not yet implemented - ring settings')

	def insert(self, rotor, is_reflector=False): # new rotors are added on the left
		if is_reflector:
			self.__reflector = rotor
		else:
			self.__rotors.append(rotor)

	def connect(self, plug1, plug2):
		self.__plugboard.connect(plug1, plug2)

	def press(self, character):
		# first, ensure character is uppercase
		character = character.upper()
		# get numerical value of character
		idx = Machine.map(character)
		if self.VERBOSE: print('PRESS:\t ' + character, idx)
		if idx is None: # unknown character
			return ''

		# rotate rotors from right to left
		for r in iter(self.__rotors[::-1]):
			# TODO: handle double stepping
			if not r.rotate(): # not turnover
				break

		# send character to plugboard
		idx = self.__plugboard.send(idx)
		if self.VERBOSE: print('PLUG\t~' + Machine.map(idx), idx)

		# Now connect the "wires"

		# pass character through each rotor (right to left)
		for r in self.__rotors[::-1]:
			idx = r.signal_in(idx)
			if self.VERBOSE: print(str(r.name) + '\t>' + Machine.map(idx), idx)

		# bounce off reflector
		if self.__reflector:
			idx = self.__reflector.signal_in(idx)
			if self.VERBOSE: print(str(self.__reflector.name) + '\t|' + Machine.map(idx), idx)

			# go through rotors again, in reverse
			for r in self.__rotors:
				idx = r.signal_out(idx)
				if self.VERBOSE: print(str(r.name) + '\t<' + Machine.map(idx), idx)

		# send back through plugboard
		idx = self.__plugboard.send(idx)
		if self.VERBOSE: print('PLUG\t~' + Machine.map(idx), idx)

		return Machine.map(idx)

	def __str__(self):
		return ('{} '*len(self.__rotors)).format(*self.__rotors)

class Rotor:
	def __init__(self, mapping, notch='', name=''):
		self.__mapping     = Machine.alphabet_map(mapping)
		self.__notch       = Machine.map(notch)
		self.__name        = name
		self.__offset      = 0
		self.__ringsetting = 0

	@property
	def size(self):
		return len(self.__mapping)

	@property
	def offset(self):
		return self.__offset
	@offset.setter
	def offset(self, o):
		self.__offset = o % self.size

	@property
	def mapping(self):
		o = self.offset
		return self.__mapping[o:] + self.__mapping[:o]

	@property
	def name(self):
		return self.__name

	def signal_in(self, idx):
		cm = self.mapping[idx]
		circular_idx = cm - self.offset
		return circular_idx % self.size

	def signal_out(self, idx):
		circular_idx = (idx + self.offset) % self.size
		return self.mapping.index(circular_idx)

	def rotate(self):
		turnover = (self.__notch == self.offset)
		self.offset += 1
		return turnover

	def __str__(self):
		return self.__name + ': ' + Machine.map(self.offset)

class Plugboard:
	def __init__(self):
		self.__connections = []

	def connect(self, plug1, plug2):
		p1 = Machine.map(plug1.upper())
		p2 = Machine.map(plug2.upper())
		if p1 != p2:
			self.__connections.append((p1, p2))

	def send(self, signal):
		output = signal
		for pair in self.__connections:
			if signal in pair: # set output to other element in pair
				output = pair[(pair.index(signal)+1) % 2]
		return output

	def __str__(self):
		return str(self.__connections)

def enigma():
	m = Machine()

	# wire plugboard
	m.connect('a', 'b')

	# initialize rotors
	#           'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
	I   = Rotor('EKMFLGDQVZNTOWYHXUSPAIBRCJ', notch='Q', name='I')
	II  = Rotor('AJDKSIRUXBLHWTMCQGZNPYFVOE', notch='E', name='II')
	III = Rotor('BDFHJLCPRTXVZNYEIWGAKMUSQO', notch='V', name='III')
	# IV  = Rotor('ESOVPZJAYQUIRHXLNFTGKDCMWB', notch='J', name='IV')
	# V   = Rotor('VZBRGITYUPSDNHLXAWMJQOFECK', notch='Z', name='V')

	# reflector
	# A = Rotor('EJMZALYXVBWFCRQUONTSPIKHGD', name='A')
	B = Rotor('YRUHQSLDPXNGOKMIEBFZCWVJAT', name='B')
	# C = Rotor('FVPJIAOYEDRZXWGCTKUQSBNMHL', name='C')

	# insert rotors
	m.insert(I)
	m.insert(II)
	m.insert(III)
	m.insert(B, is_reflector=True)

	return m

if __name__ == '__main__':
	# initialize the enigma machine
	m = enigma()
	offset = ('A','A','A')

	try: # parse input
		msg = sys.argv[1]
	except: # just press a key 10000 times
		m.VERBOSE = False
		track = ''
		for _ in range(10000):
			track += m.press('!')
		print(len(track))
		print(m)
	else: # encode and decode message
		# encode
		m.offset(*offset)
		print(m)
		enc = ''
		print('To encode: %s' % msg)
		for c in msg:
			enc += m.press(c)
			m.VERBOSE = False

		# decode
		m.offset(*offset)
		dec = ''
		print('To decode: %s [BDZGO]' % enc)
		for c in enc:
			dec += m.press(c)
		print('Decoded  : %s' % dec)
		print(m)

