#!/usr/bin/env python3

import argparse, curses, os, sys
from collections import deque
from machine import Machine, Rotor

class Enigma(Machine):
	# standard rotors
	I   = Rotor('EKMFLGDQVZNTOWYHXUSPAIBRCJ', notch='Q', name='I')
	II  = Rotor('AJDKSIRUXBLHWTMCQGZNPYFVOE', notch='E', name='II')
	III = Rotor('BDFHJLCPRTXVZNYEIWGAKMUSQO', notch='V', name='III')
	IV  = Rotor('ESOVPZJAYQUIRHXLNFTGKDCMWB', notch='J', name='IV')
	V   = Rotor('VZBRGITYUPSDNHLXAWMJQOFECK', notch='Z', name='V')

	# standard reflector
	A = Rotor('EJMZALYXVBWFCRQUONTSPIKHGD', name='A')
	B = Rotor('YRUHQSLDPXNGOKMIEBFZCWVJAT', name='B')
	C = Rotor('FVPJIAOYEDRZXWGCTKUQSBNMHL', name='C')

	def __init__(self,
	             rotors=None,      # rotor ordering
	             reflector=None,   # choose which reflector to use
	             plugboard=None,   # connect characters on plugboard
	             offset=None,      # choose starting locations
	             ringsettings=None # choose internal ring settings
	            ):
		Machine.__init__(self)

		self.VERBOSE = False

		# parse rotors string
		rotor_list = rotors.split(' ') if rotors else []
		if len(rotor_list) not in [3,4]:
			raise ValueError('Expected 3 or 4 rotors')
		if len(rotor_list) != len(set(rotor_list)):
			raise ValueError('Must use unique rings')
		for rotor in rotors.split(' '):
			if rotor not in ['I', 'II', 'III', 'IV', 'V']:
				raise ValueError('Invalid rotor')
			self.insert(eval('Enigma.'+rotor))

		# parse reflector
		if reflector not in ['A', 'B', 'C']:
			raise ValueError('Invalid reflector')
		self.insert(eval('Enigma.'+reflector), is_reflector=True)

		# connect plugboard
		try:
			connections = plugboard.split(' ') if plugboard else []
		except: # no connections
			pass
		else:
			for p1,p2 in connections:
				self.connect(p1, p2)

		# position rotors
		if offset: self.offset(*offset)

		# configure "ringstellung"
		if ringsettings: self.ringsettings(*ringsettings)

class Curses():
	def __init__(self, machine):
		self.machine = machine
		self.screen = curses.initscr()
		curses.noecho()          # turn off key echoing
		curses.cbreak()          # turn off key buffering
		curses.start_color()     # enables terminal colors
		self.screen.keypad(1)    # handle multibyte special keys
#		curses.curs_set(0)       # hide the cursor
		self.height, self.width = self.screen.getmaxyx()
		self.history = deque(maxlen=1000) # remember 1000 characters
		self.cursor = [1,1]

	def __enter__(self):
		# setup screen
		self.screen.clear()
		self.screen.border(0)
		self.update_screen(self.cursor, '', '')
		return self.curses_enigma() # the main loop

	def __exit__(self, type, value, traceback):
		self.screen.erase()
		self.screen.refresh()
		self.screen.keypad(0)
		curses.echo()
		curses.nocbreak()
		curses.endwin()          # terminate curses

	def display_rotors(self, y, x, yoff=0, xoff=0):
		y += yoff
		x += xoff
		self.screen.addstr(y, x+1, str(self.machine), curses.A_UNDERLINE)

	def display_key(self, y, x, char='', echar='', yoff=0, xoff=0):
		y += yoff
		x += xoff
		self.screen.addstr(y, x  +6, char.upper())
		self.screen.addstr(y, x+1+6, ' ')
		self.screen.addstr(y, x+2+6, '-')
		self.screen.addstr(y, x+3+6, '>')
		self.screen.addstr(y, x+4+6, ' ')
		self.screen.addstr(y, x+5+6, echar)

	def display_keyboard(self, y, x, key, yoff=0, xoff=0):
		y += yoff
		x += xoff
		def find_item(l, e): # assumes only one match
			for i in range(len(l)):
				if e in l[i]: return l[i].index(e), i
			return None, None
		keys = ['QWERTYUIOP',
				'ASDFGHJKL',
				'ZXCVBNM'
			   ]
		x1,y1 = find_item(keys, key)
		self.screen.addstr(y  , x  , ' '.join(keys[0]))
		self.screen.addstr(y+1, x+1, ' '.join(keys[1]))
		self.screen.addstr(y+2, x+2, ' '.join(keys[2]))
		if (x1 and y1) is not None:
			self.screen.addstr(y+y1, x+(x1*2)+(y1), key, curses.A_REVERSE)

	def display_history(self):
		h, w = self.height, self.width-2
		list1, list2 = zip(*list(self.history)[::-1])
		str1, str2 = ''.join(list1), ''.join(list2)
		self.screen.addstr(h-3, 1, str1[0]  , curses.A_BOLD)
		self.screen.addstr(h-3, 2, str1[1:w], curses.A_DIM)
		self.screen.addstr(h-2, 1, str2[0]  , curses.A_BOLD)
		self.screen.addstr(h-2, 2, str2[1:w], curses.A_DIM)

	def update_screen(self, cursor, char, echar):
		yoff = max((self.height//2) - 7, 0)
		xoff = max((self.width//2) - 11, 0)
		self.display_rotors(*cursor, yoff=yoff, xoff=xoff)
		self.display_keyboard(*cursor, echar, yoff=2+yoff, xoff=xoff)
		self.display_key(*cursor, char, echar, yoff=6+yoff, xoff=xoff)
		if self.history: self.display_history()
		self.screen.move(cursor[0]+6+yoff, cursor[1]+6+xoff)
		self.screen.refresh()

	def handle_resize(self, cursor):
		self.screen.clear()
		self.screen.border(0)
		self.height, self.width = self.screen.getmaxyx()
		if self.history: char,echar = self.history[0]
		else               : char,echar = '',''
		self.update_screen(cursor, char, echar)

	def curses_enigma(self):
		while True: # input loop
			c = self.screen.getch()
			if c == curses.KEY_RESIZE:
				self.handle_resize(self.cursor)
				continue
			if c == 27: break # ESC
			try              : char = chr(c)
			except ValueError: continue
			echar = self.machine.press(char)
			if not echar: continue

			self.history.append((char.upper(), echar))
			self.update_screen(self.cursor, char.upper(), echar)

if __name__ == '__main__':
	def num_string(value):
		try:
			return [int(x) for x in value.split(' ')]
		except:
			raise argparse.ArgumentTypeError('"%s" is not a list of numbers' % value)
	parser = argparse.ArgumentParser(description='Enigma machine emulator')
	parser.add_argument('-f', dest='file',
	                    help='file to encode')
	parser.add_argument('-m', dest='message',
	                    help='message to encode',
	                    default=None)
	parser.add_argument('-p',dest='plugboard',
	                    help='plugboard connections',
	                    default=None)
	parser.add_argument('-r', dest='rotors',
	                    help='rotor order',
	                    default='I II III')
	parser.add_argument('-o', dest='offset',
	                    help='rotor offset',
	                    default='AAA')
	parser.add_argument('-s', dest='settings',
	                    help='ring settings',
	                    type=num_string)
	parser.add_argument('-u', dest='reflector',
	                    help='which reflector to use',
	                    default='B')
	parser.add_argument('-v', dest='verbose',
	                    help='be verbose',
	                    action='store_true')

	args = parser.parse_args()

	# initialize the enigma machine
	m = Enigma(rotors=args.rotors,
	           reflector=args.reflector,
	           plugboard=args.plugboard,
	           offset=args.offset,
	           ringsettings=args.settings
	          )

	# choose what to do
	if args.file or args.message:
		if args.verbose: print(m)

		encoded_msg = ''
		if args.file: # encode file
			encoded_msg = ''
			with open(args.file, 'r') as f:
				for c in iter(lambda: f.read(1), ''):
					encoded_msg += m.press(c)
		elif args.message: # encode message
			if args.verbose: print('Message: %s' % args.message)
			for c in args.message:
				encoded_msg += m.press(c)

		if sys.stdout.isatty(): # print to console
			print('Encoded message: %s' % encoded_msg)
		else: # pipe/redirected output
			print(encoded_msg)

		if args.verbose: print(m)
	else: # start console interface
		# fix escape key delay
		os.environ.setdefault('ESCDELAY', '25')
		try:
			with Curses(m): pass
		except KeyboardInterrupt:
			pass # catch ctrl+c

